
call plug#begin()

Plug 'Raimondi/delimitMate'                                     " 自动加括号，换行等
Plug 'ap/vim-css-color'                                         " CSS颜色显示
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-jedi', { 'for': ['python'] }               "python补全
Plug 'klen/python-mode', { 'for': ['python'] }
Plug 'SirVer/ultisnips'                                         " 代码块
Plug 'honza/vim-snippets'                                       " 常用代码块集合
Plug 'vim-ctrlspace/vim-ctrlspace'                              " buffer,tab列表
Plug 'jlanzarotta/bufexplorer'
Plug 'tpope/vim-surround'
Plug 'godlygeek/tabular'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'tpope/vim-repeat'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle' }
Plug 'scrooloose/nerdcommenter'                                 " 注释插件
Plug 'Lokaltog/vim-easymotion'
Plug 'altercation/vim-colors-solarized'                         " themes
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'NLKNguyen/papercolor-theme'
Plug 'tomasr/molokai' 
Plug 'neomake/neomake'                                          " 代码检错
Plug 'tpope/vim-fugitive'                                       " git 
Plug 'Yggdroot/indentLine'                                      " 缩进线
Plug 'ekalinin/Dockerfile.vim'                                  " Dockerfile
Plug 'Lokaltog/vim-easymotion'
Plug 'majutsushi/tagbar'
Plug 'mattn/emmet-vim'                                          " Emmet神器
Plug 'ap/vim-css-color'                                         " CSS颜色显示
Plug 'jrosiek/vim-mark'                                         " 高亮，看源码神器


call plug#end()

set mouse=a

" file settings
syntax on
filetype on
filetype plugin on
filetype indent on
set encoding=utf-8
set fileformat=unix
set fileencodings=utf-8,chinese,latin-1,gbk
set nocompatible
set nobackup
set noswapfile
set clipboard=unnamed,unnamedplus
set hidden

set nu!
let mapleader=","
set timeoutlen=300
nnoremap n nzzzv
nnoremap N Nzzzv
set number
set t_Co=256
set pastetoggle=<F5>                                                                                       
set smartindent                   "开始自动缩进                                                            
set shiftwidth=2                  "缩进宽度                                                                
set backspace=indent,eol,start    "backspace设置                                                           
set tabstop=2                     "tab宽度为4个空格                                                        
set et                            "编辑时将tab替换为空格                                                   
set smarttab                      "一次backspace删除多个空格                                               
set incsearch                                                                                              
set hlsearch                      "高亮搜索                                                                
set wildmenu
" set lines=45 columns=110                                                                                   

" colorscheme
set background=dark
"colorscheme solarized
"colorscheme PaperColor
colorscheme desert
" menu color
highlight! Pmenu ctermbg=254 guibg=#444444
highlight! PmenuSel ctermfg=7 ctermbg=4 guibg=#5f87ff guifg=#eeeeee
" #555555

" --------------------------- short keys ------------------------------------- 

"easier navigation between split windows
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

"switch buffer
map <F1> :bp!<cr>
map <F2> :bn!<cr>

"搜索时把当前结果置于屏幕中央并打开折叠
nnoremap n nzzzv
nnoremap N Nzzzv

"map ESC with jj
inoremap jj <ESC>


" --------------------------- deoplete ncl -----------------------------------
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_start_length = 2
let g:deoplete#auto_complete_delay = 50
"let g:deoplete#omni_patterns = {}
let g:deoplete#file#enable_buffer_path = 1
"call deoplete#custom#option('omni_patterns', {
"\ 'ncl': '[^. *\t]\.\w*\/',
"\})
set omnifunc=syntaxcomplete#Complete
set complete+=k
set completeopt+=longest
au BufRead,BufNewFile *.ncl set filetype=ncl
au BufRead,BufNewFile *.ncl setlocal dictionary+=~/.config/nvim/dictionary/ncl.dic
"au BufRead,BufNewFile *.ncl setlocal dictionary+=C:\Users\sutaodesk\AppData\Local\nvim\dictionary\ncl.dic
au BufRead,BufNewFile *.ncl setlocal commentstring=;%s

let g:deoplete#sources = {'_':['file','buffer','omnifunc'],'ncl':['file','omnifunc','buffer','dictionary']}

call deoplete#custom#option('keyword_patterns', {
\ '_': '[a-zA-Z_]\k*',
\ 'ncl': '[a-zA-Z_]\w*',
\ 'ruby': '[a-zA-Z_]\w*[!?]?',
\})

"
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><BS> deoplete#smart_close_popup()."\<C-h>"
" 隐藏preview窗口
set completeopt-=preview

" fortran

let g:python2_host_prog = '/usr/local/bin/python'
let g:python3_host_prog = '/usr/local/bin/python3'
"let g:python2_host_prog = '/usr/local/bin/python'
" xps13
"let g:python3_host_prog = 'D:/ProgramData/Anaconda3/python.exe'
" sutaodesk
"let g:python3_host_prog = 'C:\ProgramData\Anaconda3\python.exe' 

" python

" ----- python ---
autocmd FileType python set shiftwidth=4 | set expandtab | set tabstop=4
let g:pymode_lint_on_write = 0

" --------------------------- indentLine -------------------------------------
let g:indentLine_enabled = 1				" 开启indentLine插件
let g:indentLine_color_term = 250		" 设置缩进线颜色
let g:indentLine_char = '¦' 				" 设置缩进线 字符 ¦

" --------------------------- airline ----------------------------------------
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
" let g:airline_powerline_fonts = 1
let g:airline_theme = "solarized"
let g:airline#extensions#tabline#show_buffers = 1 "不显示buffer set to 0
" show tab number in tab line
let g:airline#extensions#tabline#tab_nr_type = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline_section_b = '  %{strftime("%Y-%m-%d %H:%M")}  BN: %{bufnr("%")}  '
let g:airline_section_z = ' %l/%L  %3v '
let g:airline_section_z = airline#section#create(['%3p%%', ' %5l/%L', '  %3v'])
let g:airline_theme="sol"
let g:airline_detect_whitespace          = 0 "关闭空白符检测
set ttimeoutlen=0
set laststatus=2
let g:airline_extensions = ['tabline']
let g:airline_symbols_ascii = 1

" nerdtree
map <C-n> :NERDTreeToggle<CR>

" delimitMate
let delimitMate_expand_cr=1
let delimitMate_matchpairs = "(:),[:],{:}"
au FileType python let b:delimitMate_nesting_quotes = ['"']
" 关闭某些类型文件的自动补全
au FileType vim let b:delimitMate_autoclose = 0

" EasyMotion
hi EasyMotionTarget ctermbg=253 ctermfg=0
hi EasyMotionTarget2First ctermbg=253 ctermfg=0
hi EasyMotionTarget2Second ctermbg=253 ctermfg=0
let g:EasyMotion_leader_key = '<Leader>'
let g:EasyMotion_mapping_n = ''

" Rainbow()
au VimEnter * RainbowParentheses

" ultisnips 防止和deoplete冲突
let g:UltiSnipsExpandTrigger="<s-tab>"                                            
let g:UltiSnipsJumpForwardTrigger="<s-tab>"  
