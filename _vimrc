
" -----------------------------------------------------------------------------
"  < 判断操作系统是否是 Windows 还是 Linux >
" -----------------------------------------------------------------------------
let g:iswindows = 0
let g:islinux = 0
if(has("win32") || has("win64") || has("win95") || has("win16"))
    let g:iswindows = 1
else
    let g:islinux = 1
endif
" -----------------------------------------------------------------------------
"  < 判断是终端还是 Gvim >
" -----------------------------------------------------------------------------
if has("gui_running")
    let g:isGUI = 1
else
    let g:isGUI = 0
endif

" -----------------------------------------------------------------------------
" 设置xterm
" -----------------------------------------------------------------------------
if g:islinux
  set clipboard=exclude:.*    "vim -X ; default autoselect,exclude:cons\|linux
  "set guifont=YaheiMonaco\ 12
  "set guifont=Courier\ 10\ Pitch\ 12
  if has('mouse')
    set mouse=a
  endif
endif


let &t_ZH="\e[3m"
let &t_ZR="\e[23m"
set t_ZH=[3m
set t_ZR=[23m

"------------------------------------------------------------------------------
"设置编码和界面
"------------------------------------------------------------------------------
set encoding=utf-8
set fileformat=unix
set fileencodings=utf-8,chinese,latin-1,gbk "设置支持打开的文件的编码
if g:iswindows
  set fileencoding=utf-8
  "set guifont=courier_new:h11
  set guifont=consolas:h11
  "以下三行模拟Windows操作，如Ctrl-C复制
  "source $VIMRUNTIME/vimrc_example.vim
  "source $VIMRUNTIME/mswin.vim
  "behave mswin
else
  set fileencoding=utf-8
endif

if (g:iswindows && g:isGUI)
  "解决菜单乱码
  source $VIMRUNTIME/delmenu.vim
  source $VIMRUNTIME/menu.vim
  "解决consle输出乱码
  language messages zh_CN.utf-8
endif

"不生成备份
set nobackup
set noswapfile
"------------------------------------------------------------------------------
"输入和界面配置
"------------------------------------------------------------------------------

" Ctrl + K 插入模式下光标向上移动
imap <c-k> <Up>
" Ctrl + J 插入模式下光标向下移动
imap <c-j> <Down>
" Ctrl + H 插入模式下光标向左移动
imap <c-h> <Left>
" Ctrl + L 插入模式下光标向右移动
imap <c-l> <Right>

"easier navigation between split windows
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

"Set mapleader
let mapleader = ","
set timeoutlen=300

"switch buffer
map <F1> :bp!<cr>
map <F2> :bn!<cr>

"switch tabs
nnoremap <leader>=  :tabnext<CR>
nnoremap <leader>-  :tabprev<CR>
 
"搜索时把当前结果置于屏幕中央并打开折叠
nnoremap n nzzzv
nnoremap N Nzzzv

"map ESC with jj
inoremap jj <ESC>

syntax on
set pastetoggle=<F5>
set t_Co=256
set smartindent                   "开始自动缩进
set shiftwidth=2                  "缩进宽度
set backspace=indent,eol,start    "backspace设置
set tabstop=2                     "tab宽度为4个空格
set et                            "编辑时将tab替换为空格
set smarttab                      "一次backspace删除多个空格
set incsearch
set hlsearch                      "高亮搜索
set lines=40 columns=110
set nu!
set clipboard=unnamed,unnamedplus

"ctags
"set tags=/public/home/SuTao/local/obsproc_prep.v3.8.0_src/sorc/tags



"------------------------------------------------------------------------------
"  Vundle
"------------------------------------------------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
if g:islinux
  set rtp+=$MY_VIM_PATH/vimfiles/bundle/Vundle.vim
  set rtp+=$MY_VIM_PATH/vimfiles/doc/nclhelpdoc
  call vundle#begin('$MY_VIM_PATH/vimfiles/bundle')
else
  set rtp+=$HOME/vimfiles/bundle/vundle.vim
  set rtp+=$HOME/vimfiles/doc/nclhelpdoc
  call vundle#begin('$HOME/vimfiles/bundle/')
endif
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'python-mode/python-mode'
Plugin 'davidhalter/jedi-vim'
Plugin 'bsdelf/bufferhint'
Plugin 'bling/vim-airline'
Plugin 'Shougo/neocomplete.vim'          "关键字补全
Plugin 'SirVer/ultisnips'                "代码补齐
Plugin 'honza/vim-snippets'              "默认snippets
Plugin 'Yggdroot/indentLine' "虚线缩进
Plugin 'Lokaltog/vim-easymotion'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'speeddating.vim'
Plugin 'repeat.vim'
Plugin 'matchit.zip'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'scrooloose/syntastic'
Plugin 'majutsushi/tagbar'
Plugin 'morhetz/gruvbox'
Plugin 'lervag/vimtex'
Plugin 'xuhdev/vim-latex-live-preview'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'iamcco/markdown-preview.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
set nocompatible
set showcmd


" -----------------------------------------------------------------------------
" ncl自动补全
" -----------------------------------------------------------------------------
set complete-=k complete+=k " Add dictionary search (as per dictionary option)
set wildmode=list:full
set wildmenu
if g:islinux
  au BufRead,BufNewFile *.ncl set dictionary=$MY_VIM_PATH/vimfiles/dictionary/ncl.dic
  set rtp+=$MY_VIM_PATH/vimfiles
else
  au BufRead,BufNewFile *.ncl set dictionary=$VIM\vimfiles\dictionary\ncl.dic
endif
au BufRead,BufNewFile *.ncl set filetype=ncl
"au BufRead,BufNewFile *.ncl setlocal dictionary+=C:\Users\sutaodesk\AppData\Local\nvim\dictionary\ncl.dic

" -----------------------------------------------------------------------------
" fortran设置
" -----------------------------------------------------------------------------
let s:extfname= expand("%:e")
if s:extfname==? "f90"
    let fortran_free_source=1
   unlet! fortran_fixed_source
else
    let fortran_fixed_source=1
   unlet! fortran_free_source
endif

let fortran_more_precise=1
let fortran_do_enddo=1
"折叠方式
"set foldmethod=syntax
"允许折叠
"let fortran_fold=1
"let fortran_fold_conditionals=1
let fortran_have_tabs=1

" -----------------------------------------------------------------------------
" python
" -----------------------------------------------------------------------------
autocmd FileType python set shiftwidth=4 | set expandtab | set tabstop=4
" python-mode pymode
let g:pymode_rope = 0 "不使用rope自动补全
"Linting
let g:pymode_lint = 1
let g:pymode_lint_on_write = 1
"let g:pymode_lint_on_fly = 1
let g:pymode_debug=1
let g:pymode_lint_checkers = ['pylint', 'pep8']
" -----------------------------------------------------------------------------
"  < ctrlp.vim 插件配置 >
" -----------------------------------------------------------------------------
" 一个全路径模糊文件，缓冲区，最近最多使用，... 检索插件；详细帮助见 :h ctrlp
" 常规模式下输入：Ctrl + p 调用插件

" -----------------------------------------------------------------------------
"  < airline 插件配置 >
" -----------------------------------------------------------------------------
" 状态栏增强
let g:airline#extensions#tabline#enabled = 1 "打开tabline
let g:airline#extensions#tabline#show_buffers = 0 "不显示buffer
let g:airline_section_b = '   %{strftime("%Y-%m-%d %H:%M")}     BN: %{bufnr("%")}  '
let g:airline_theme="sol"
let g:airline_detect_whitespace          = 0 "关闭空白符检测
set ttimeoutlen=0
set laststatus=2

" -----------------------------------------------------------------------------
" 文件浏览器nerd tree
" -----------------------------------------------------------------------------
map <C-n> :NERDTreeToggle<CR>
 
" -----------------------------------------------------------------------------
" 注释工具 NERD_commenter
" -----------------------------------------------------------------------------
",cc      注释当前行
",cu      取消注释
",c(空格) 切换注释/非注释状态
",ci      分行切换注释状态
"Normal模式下，几乎所有命令前面都可以指定行数
"Visual模式下执行命令，会对选中的特定区块进行注释/反注释

" buffer hint
nnoremap - :call bufferhint#Popup()<CR>
nnoremap \ :call bufferhint#LoadPrevious()<CR>

" -----------------------------------------------------------------------------
" Easymotion
" -----------------------------------------------------------------------------
let g:EasyMotion_leader_key = '<Leader>'
let g:EasyMotion_mapping_n = ''
map <leader>h <Plug>(easymotion-linebackward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <leader>l <Plug>(easymotion-lineforward)
" 重复上一次操作, 类似repeat插件, 很强大
map <leader>. <Plug>(easymotion-repeat)

" -----------------------------------------------------------------------------
" ctrlp 快速打开文件，当前目录及以下的文件
" -----------------------------------------------------------------------------
"搜索隐藏文件
let g:ctrlp_show_hidden = 1
 


" -----------------------------------------------------------------------------
" 自动补全 neocomplete
" -----------------------------------------------------------------------------
nmap <F3> :NeoCompleteEnable<CR>
imap <F3> <ESC>:NeoCompleteEnable<CR>a
nmap <F4> :NeoCompleteToggle<CR>      
imap <F4> <ESC>:NeoCompleteToggle<CR>a
"If Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use smartcase.
let g:neocomplete#enable_smart_case = 0
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#manual_completion_start_length = 2
" <TAB>: completion.
let g:neocomplete#disable_auto_complete = 1
set completeopt+=longest
" For smart TAB completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" :
        \ <SID>check_back_space() ? "\<TAB>" :
        \ neocomplete#start_manual_complete() 
        "\ neocomplete#start_manual_complete()."\<C-p>" 
  function! s:check_back_space() "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
  endfunction"}}}
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
inoremap <expr><C-g>     neocomplete#undo_completion()

"disable python
"if !exists('g:neocomplete#sources#omni#input_patterns')
  "let g:neocomplete#sources#omni#input_patterns = {}
"endif
"let g:neocomplete#sources#omni#input_patterns.python = ''
"let g:neocomplete#sources#omni#input_patterns.ruby = ''
" --jedi-vim
if !exists('g:neocomplete#force_omni_input_patterns')
    let g:neocomplete#force_omni_input_patterns = {}
endif
autocmd FileType python setlocal omnifunc=jedi#completions
let g:jedi#completions_enabled = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#smart_auto_mappings = 0
let g:jedi#documentation_command = "U"
let g:neocomplete#force_omni_input_patterns.python = 
      \ '\%([^. \t]\.\|^\s*@\|^\s*from\s.\+import \|^\s*from \|^\s*import \)\w*'
" alternative pattern: '\h\w*\|[^. \t]\.\w*'

" -----------------------------------------------------------------------------
" ultisnip 代码段补全
" -----------------------------------------------------------------------------
let g:UltiSnipsSnippetDirectories=["UltiSnips"]
"let g:UltiSnipsSnippetsDir='~/local/vimfiles/bundle/mysnips'
let g:UltiSnipsExpandTrigger="<Leader>a"         "开始补全
let g:UltiSnipsJumpForwardTrigger="<Leader>s"    "补全下一处
let g:UltiSnipsJumpBackwardTrigger="<Leader>z"   "补全上一处
 
" -----------------------------------------------------------------------------
" syntastic 语法检测
" -----------------------------------------------------------------------------
let g:syntastic_mode_map = { 'mode': 'passive',
    \ 'active_filetypes': [],
    \ 'passive_filetypes': ['python'] }
nmap <Leader>c :SyntasticCheck<CR>

" -----------------------------------------------------------------------------
" tagbar 函数列表
" -----------------------------------------------------------------------------
nmap <F8> :TagbarToggle<CR>


" colorscheme
syntax enable
set background=dark
"colorscheme molokai
colorscheme gruvbox
"colorscheme default
let g:gruvbox_contrast_dark = 'hard'


" latex
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'

let g:livepreview_previewer = 'zathura'
let g:livepreview_cursorhold_recompile = 0
let g:livepreview_engine = 'xelatex'

" markdown
let g:indentLine_concealcursor = ''
let g:vim_markdown_conceal=0
